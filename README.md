Nakama client library for Godot (in GDScript)
=============================================

This is a small addon to the [Godot](https://godotengine.org) game engine,
written in GDScript, that allows interacting with
[Nakama](https://heroiclabs.com), an Open Source game backend, using
both Nakama's REST and realtime APIs.

Nakama provides its own C# library, that may work with Godot's C# build,
but this library will work for folks using the "classic" Godot build
(which doesn't have C# support) and it works on all the platforms that Godot
supports, including those that don't currently support C# (like HTML5 and iOS).

This library is also a little more Godot-ish, so it might be easier to use
for Godot folks.

Installation
------------

### Via the Asset Library ###

1. Open your project in Godot
2. Click "AssetLib" in the top middle of the window
3. Search for "Nakama Client in GDScript"
4. Click the item
5. Click the "Download" button
6. Click the "Install" button
6. Select the "addons" directory (and only the "addons" directory)
7. Click the "Install" button
8. Go to "Project" -> "Project settings" -> "Plugins" and enable this plugin

### Manually ###

1. Download the latest code
2. Copy the files files in 'addons/nakama-client' to your projects 'addons/' directory
3. Go to "Project" -> "Project settings" -> "Plugins" and enable this plugin

Using the client
----------------

First, you need to add a `NakamaRestClient` node to your scene:

1. Click the plus button in the "Scene" tab
2. Find and add a "NakamaRestClient" node
3. Using the inspector, configure the node for your Nakama server

Then, in a script, you'll need to authenticate a user somehow. For example:

```
var promise = $NakamaRestClient.authenticate_email("email@example.com", "pasword", true, "username")
promise.error == OK and yield(promise, "completed")
print(promise.response)
```

Now, you'll be able to make any other Nakama API calls! If you want to
use the realtime API, you'll need to first create a realtime client:

```
var realtime_client = $NakamaRestClient.create_realtime_client()
yield(realtime_client, "connected")

promise = realtime_client.send({ "status_update": { "status": "I'm here!" }})
promise.error == OK and yield(promise, "completed")
print (promise.response) 
```

For the realtime client to actually work, you'll need to periodically call
`realtime_client.poll()`, for example, in your `_process()` method:

```
func _process(delta: float) -> void:
	if realtime_client:
		realtime_client.poll()
```

Signals, Promises and Co-routines
---------------------------------

All the API methods on `NakamaRestClient` have corresponding signals for
when the call has completed, for example, `authenticate_email()` will emit
the 'authenticate_email_completed' signal.

If you have a very simple use of Nakama, where only one thing will ever call
`authenticate_email()` then connecting to this signal is a pretty Godot-ish
way to work with the client.

For example:

```
$NakamaRestClient.connect('authenticate_email_complete', self, '_on_authenticated')
$NakamaRestClient.authenticate_email(...)
```

However, all the API calls, also return a promise object, which has it's own
'completed' signal. You can connect to this signal to get the response for
just this specific API call.

```
var promise = $NakamaRestClient.authenticated_email(...)
if promise.error == OK:
	promise.connect('completed', self, '_on_authenticed')
else:
	print("Unable to even make request")
```

It's a good idea to check `promise.error` to see if it has a value other
than `OK`, which indicates that the request couldn't even be started.
However, even if you forget to do this, the 'completed' signal will still
be emitted, but it won't happen until at least the next frame which
wastes a little time. By checking `promise.error` you can avoid messing
with the signal at all.

Since Godot supports coroutines via `yield()` you can also write code that
feels synchronous (even though it's actually asynchronous) like this:

```
var promise = $NakamaRestClient.authenticate_email("email@example.com", "pasword", true, "username")
promise.error == OK and yield(promise, "completed")
print(promise.response)
```

It's a good idea to use `promise.error == OK and ...` bit, because that
will avoid `yield()`ing at all, in the case that the request couldn't even
be sent. However, again, not using it won't break your code or anything,
because the signal will always be emitted.

Future
======

Someday, I'd like to make a C++ version of this plugin as a Godot module. This
would allow using gRPC instead of REST, which would be a little faster. It
would also allow using the Nakama Multiplayer API as a backend for Godot's
High-level Multiplayer API.

